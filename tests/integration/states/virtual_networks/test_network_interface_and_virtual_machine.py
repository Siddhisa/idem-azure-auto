import uuid

import pytest


@pytest.mark.skip(
    reason="Test will be disabled until module is refactored with new design changes to support caching"
)
@pytest.mark.asyncio
async def test_network_interface_and_virtual_machine(
    hub,
    ctx,
    resource_group_fixture,
    network_security_group_fixture,
    public_ip_address_fixture,
    subnet_fixture,
):
    """
    This test provisions a network interface and vm, describes them and deletes them.
    """
    # Create network interface
    resource_group_name = resource_group_fixture.get("name")
    security_group_id = network_security_group_fixture.get("id")
    public_ip_address_id = public_ip_address_fixture.get("id")
    subnet_id = subnet_fixture.get("id")
    network_interface_name = "idem-test-security-group-" + str(uuid.uuid4())
    nic_parameters = {
        "location": "eastus",
        "properties": {
            "enableAcceleratedNetworking": False,
            "hostedWorkloads": [],
            "networkSecurityGroup": {"id": security_group_id},
            "ipConfigurations": [
                {
                    "name": "idem-test-ip-configuration-" + str(uuid.uuid4()),
                    "properties": {
                        "primary": True,
                        "publicIPAddress": {"id": public_ip_address_id},
                        "subnet": {"id": subnet_id},
                    },
                }
            ],
        },
    }

    nic_ret = await hub.states.azure.virtual_networks.network_interfaces.present(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        parameters=nic_parameters,
    )
    assert nic_ret["result"], nic_ret["comment"]
    assert not nic_ret["changes"].get("old") and nic_ret["changes"]["new"]

    nic_wait_ret = await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/networkInterfaces/{network_interface_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
    hub.tool.azure.resource.check_response_payload(nic_parameters, nic_wait_ret["ret"])

    # Describe network interface
    describe_ret = await hub.states.azure.virtual_networks.network_interfaces.describe(
        ctx
    )
    assert nic_wait_ret["ret"].get("id") in describe_ret

    # Create VM
    vm_name = "idem-test-virtual-machine-" + str(uuid.uuid4())
    vm_parameters = {
        "location": "eastus",
        "properties": {
            "hardwareProfile": {"vmSize": "Standard_D2s_v3"},
            "storageProfile": {
                "imageReference": {
                    "sku": "16.04-LTS",
                    "publisher": "Canonical",
                    "version": "latest",
                    "offer": "UbuntuServer",
                },
                "osDisk": {
                    "caching": "ReadWrite",
                    "managedDisk": {"storageAccountType": "Premium_LRS"},
                    "name": "idem-test-os-disk-" + str(uuid.uuid4()),
                    "createOption": "FromImage",
                },
            },
            "osProfile": {
                "adminUsername": "adminUsername",
                "computerName": "idem-test-compute-" + str(uuid.uuid4()),
                "adminPassword": "admin-password$A",
                "linuxConfiguration": {
                    "provisionVMAgent": True,
                    "patchSettings": {"assessmentMode": "ImageDefault"},
                },
            },
            "networkProfile": {
                "networkInterfaces": [
                    {
                        "id": nic_wait_ret["ret"].get("id"),
                        "properties": {"primary": True},
                    }
                ]
            },
        },
    }

    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        vm_name=vm_name,
        parameters=vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    del vm_parameters.get("properties").get("osProfile")["adminUsername"]
    del vm_parameters.get("properties").get("osProfile")["adminPassword"]
    assert not vm_ret["changes"].get("old") and vm_ret["changes"]["new"]

    vm_wait_ret = await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}?api-version=2021-07-01",
        retry_count=20,
        retry_period=10,
    )
    hub.tool.azure.resource.check_response_payload(vm_parameters, vm_wait_ret["ret"])

    # Describe virtual machine
    describe_ret = await hub.states.azure.compute.virtual_machines.describe(ctx)

    assert vm_wait_ret["ret"].get("id").lower() in [
        key.lower() for key in describe_ret.keys()
    ]

    # Delete virtual machine
    vm_del_ret = await hub.states.azure.compute.virtual_machines.absent(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        vm_name=vm_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert vm_del_ret["changes"]["old"] and not vm_del_ret["changes"].get("new")
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}?api-version=2021-07-01",
        retry_count=20,
        retry_period=10,
    )

    # Delete network interface
    del_ret = await hub.states.azure.virtual_networks.network_interfaces.absent(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
    )
    assert del_ret["result"], del_ret["comment"]
    assert del_ret["changes"]["old"] and not del_ret["changes"].get("new")
    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/networkInterfaces/{network_interface_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
