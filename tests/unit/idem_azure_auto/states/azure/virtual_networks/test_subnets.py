import copy
from collections import ChainMap

import pytest

RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
VIRTUAL_NETWORK_NAME = "my-vnet"
SUBNET_NAME = "my-subnet"
RESOURCE_PARAMETERS = {
    "address_prefix": "10.12.13.0/27",
    "enforce_private_link_endpoint_network_policies": True,
    "delegations": [{"name": "my-delegation", "service": "Microsoft.CloudTest/images"}],
    "enforce_private_link_service_network_policies": True,
    "service_endpoints": ["Microsoft.Sql"],
    "service_endpoint_policy_ids": [
        "/subscriptions/subscriptionId/resourceGroups/resourceGroupName/providers/Microsoft.Network/serviceEndpointPolicies/serviceEndpointPolicyName"
    ],
}
RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "addressPrefix": "10.12.13.0/27",
        "delegations": [
            {
                "name": "my-delegation",
                "properties": {"serviceName": "Microsoft.CloudTest/images"},
            }
        ],
        "privateEndpointNetworkPolicies": "Enabled",
        "privateLinkServiceNetworkPolicies": "Enabled",
        "serviceEndpoints": [{"service": "Microsoft.Sql"}],
        "serviceEndpointPolicies": [
            {
                "id": "/subscriptions/subscriptionId/resourceGroups/resourceGroupName/providers/Microsoft.Network/serviceEndpointPolicies/serviceEndpointPolicyName"
            }
        ],
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of subnets. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.virtual_networks.subnets.present = (
        hub.states.azure.virtual_networks.subnets.present
    )
    mock_hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present = (
        hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present
    )
    mock_hub.tool.azure.virtual_networks.subnets.convert_present_to_raw_subnets = (
        hub.tool.azure.virtual_networks.subnets.convert_present_to_raw_subnets
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}/subnets/{SUBNET_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.virtual_networks.subnets",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        assert SUBNET_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        assert SUBNET_NAME in url
        assert json == RESOURCE_PARAMETERS_RAW
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.virtual_networks.subnets.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        VIRTUAL_NETWORK_NAME,
        SUBNET_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.virtual_networks.subnets '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters
    # Test present() with --test flag off
    ret = await mock_hub.states.azure.virtual_networks.subnets.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        VIRTUAL_NETWORK_NAME,
        SUBNET_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert f"Created azure.virtual_networks.subnets '{RESOURCE_NAME}'" in ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of subnets. When a resource exists, 'present' will return the existing resource. Since Azure
     subnets doesn't support PATCH operation
    """
    mock_hub.states.azure.virtual_networks.subnets.present = (
        hub.states.azure.virtual_networks.subnets.present
    )
    mock_hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present = (
        hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present
    )
    mock_hub.tool.azure.virtual_networks.subnets.update_subnets_payload = (
        hub.tool.azure.virtual_networks.subnets.update_subnets_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {
        "address_prefix": "10.12.14.0/27",
        "enforce_private_link_endpoint_network_policies": True,
        "enforce_private_link_service_network_policies": True,
        "delegations": [
            {"name": "my-delegation2", "service": "Microsoft.CloudTest/images"}
        ],
        "service_endpoints": ["Microsoft.Storage"],
        "service_endpoint_policy_ids": [
            "/subscriptions/subscriptionId/resourceGroups/resourceGroupName/providers/Microsoft.Network/serviceEndpointPolicies/serviceEndpointPolicyName"
        ],
    }

    resource_parameters_update_raw = {
        "properties": {
            "addressPrefix": "10.12.14.0/27",
            "delegations": [
                {
                    "name": "my-delegation2",
                    "properties": {"serviceName": "Microsoft.CloudTest/images"},
                }
            ],
            "privateEndpointNetworkPolicies": "Enabled",
            "privateLinkServiceNetworkPolicies": "Enabled",
            "serviceEndpoints": [{"service": "Microsoft.Storage"}],
            "serviceEndpointPolicies": [
                {
                    "id": "/subscriptions/subscriptionId/resourceGroups/resourceGroupName/providers/Microsoft.Network/serviceEndpointPolicies/serviceEndpointPolicyName"
                }
            ],
        },
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}/subnets/{SUBNET_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}/subnets/{SUBNET_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        assert SUBNET_NAME in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.virtual_networks.subnets.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        VIRTUAL_NETWORK_NAME,
        SUBNET_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.virtual_networks.subnets '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.virtual_networks.subnets.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        VIRTUAL_NETWORK_NAME,
        SUBNET_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of subnets. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.virtual_networks.subnets.absent = (
        hub.states.azure.virtual_networks.subnets.absent
    )
    mock_hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present = (
        hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        assert SUBNET_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.virtual_networks.subnets.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VIRTUAL_NETWORK_NAME, SUBNET_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.virtual_networks.subnets '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of subnets. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.virtual_networks.subnets.absent = (
        hub.states.azure.virtual_networks.subnets.absent
    )
    mock_hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present = (
        hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}/subnets/{SUBNET_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert VIRTUAL_NETWORK_NAME in url
        assert SUBNET_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.virtual_networks.subnets.absent(
        test_ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VIRTUAL_NETWORK_NAME, SUBNET_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.virtual_networks.subnets '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.virtual_networks.subnets.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, VIRTUAL_NETWORK_NAME, SUBNET_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert f"Deleted azure.virtual_networks.subnets '{RESOURCE_NAME}'" in ret["comment"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of virtual networks.
    """
    mock_hub.states.azure.virtual_networks.subnets.describe = (
        hub.states.azure.virtual_networks.subnets.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present = (
        hub.tool.azure.virtual_networks.subnets.convert_raw_subnets_to_present
    )

    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Network/virtualNetworks/{VIRTUAL_NETWORK_NAME}/subnets/{SUBNET_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.virtual_networks.subnets.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.virtual_networks.subnets.present" in ret_value.keys()
    described_resource = ret_value.get("azure.virtual_networks.subnets.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert SUBNET_NAME == old_state.get("subnet_name")
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert VIRTUAL_NETWORK_NAME == old_state.get("virtual_network_name")
        assert expected_old_state["address_prefix"] == old_state.get("address_prefix")
        assert expected_old_state[
            "enforce_private_link_endpoint_network_policies"
        ] == old_state.get("enforce_private_link_endpoint_network_policies")
        assert expected_old_state[
            "enforce_private_link_service_network_policies"
        ] == old_state.get("enforce_private_link_service_network_policies")
        assert expected_old_state["delegations"] == old_state.get("delegations")
        assert expected_old_state["service_endpoints"] == old_state.get(
            "service_endpoints"
        )
        assert expected_old_state["service_endpoint_policy_ids"] == old_state.get(
            "service_endpoint_policy_ids"
        )

    if new_state:
        assert SUBNET_NAME == new_state.get("subnet_name")
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert VIRTUAL_NETWORK_NAME == new_state.get("virtual_network_name")
        assert expected_new_state["address_prefix"] == new_state.get("address_prefix")
        assert expected_new_state[
            "enforce_private_link_endpoint_network_policies"
        ] == new_state.get("enforce_private_link_endpoint_network_policies")
        assert expected_new_state[
            "enforce_private_link_service_network_policies"
        ] == new_state.get("enforce_private_link_service_network_policies")
        assert expected_new_state["delegations"] == new_state.get("delegations")
        assert expected_new_state["service_endpoints"] == new_state.get(
            "service_endpoints"
        )
        assert expected_new_state["service_endpoint_policy_ids"] == new_state.get(
            "service_endpoint_policy_ids"
        )
